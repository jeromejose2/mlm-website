


(function(){
	var GlobalButtonBehavior = (function(){
		var _green = '#8cc739';
		var _gray = '#ccc';

		function main(){
			console.log('GlobalButtonBehavior');
		}

		main.prototype.onFocusIn = function(){
			$('fieldset select, select').focusin(function() {
				$(this).parent().css({
					'transition': 'all 0.3s ease 0s',
		    		'border': '1px solid '.concat(_green)
				});
			});
		}

		main.prototype.onFocusOut = function(){
			$('fieldset select, select').focusout(function() {
				$(this).parent().css({
					'transition': 'all 0.3s ease 0s',
		    		'border': '1px solid '.concat(_gray)
				});
			});
		}
		return main;
	})();

	console.log( GlobalButtonBehavior );

	var gbb = new GlobalButtonBehavior();
		gbb.onFocusIn();
		gbb.onFocusOut();

})();
