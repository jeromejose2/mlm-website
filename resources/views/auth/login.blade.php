@extends('layouts.master')

@section('content')

<div class="mcont cf">


    <div class="login-wrap">

    <div class="marker-line cf">
        <div class="blue"></div>
        <div class="green"></div>
        <div class="gray"></div>
    </div>

        <div class="panel-title">
            <span class="page-solo">Log in to your Account</span>
        </div>
        
        <div class="panel-body">
            <!-- <form> -->
            {!! Form::open(['url'=>url('auth/login'), 'class'=>'form-horizontal', 'method'=>'POST', 'id'=>'sender-form', 'onsubmit'=>'','enctype'=>'multipart/form-data']) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                <!--email-->
                <div class="form-group">
                    {!! Form::text('email',old('email'),['required','type'=>'email','class'=>'form-control','placeholder' => 'Email Address'])!!}
                </div>
                
                <!--password-->
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                
                <!--submit-->
                <div class="form-group no-btm">
                    {!! Form::submit('LOG IN', array('class'=>'btn-submit')) !!}
                </div>
                
                <!-- action for creating new account or reset password -->
                <div class="create-new"> 
                    <div class="action-forgot">
                        <a href="{{url('/auth/register')}}">Reset Password</a>
                    </div>
                    <div class="action-signup">
                        <a href="{{url('/auth/register')}}">Sign Up</a>
                    </div>
                </div>
                <!-- </form> -->
            {!! Form::close() !!}
        </div>

    </div>     
<!--mcont end-->
</div>

@endsection
