    @extends('layouts.master')

    @section('content')

    <div class="container-fluid">
    <div class="row">
    <div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
    <div class="panel-heading">Register</div>
    <div class="panel-body">



    <form class="form-horizontal" role="form" method="POST" action="/auth/register">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
    <label class="col-md-4 control-label">Name</label>
    <div class="col-md-6">
    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
    </div>
    </div>

    <div class="form-group">
     <label class="col-md-4 control-label">E-Mail Address</label>
    <div class="col-md-6">
    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
    </div>
    </div>

    <div class="form-group">
    <label class="col-md-4 control-label">Password</label>
    <div class="col-md-6">
    <input type="password" class="form-control" name="password">
    </div>
    </div>

    <div class="form-group">
    <label class="col-md-4 control-label">Confirm Password</label>
    <div class="col-md-6">
    <input type="password" class="form-control" name="password_confirmation">
    </div>
    </div>

    <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
    <button type="submit" class="btn btn-primary">
    Register
    </button>

    </div>
    </div>
    </form>


    <p>sample payment after BANK</p>

    <div class="pageContent">
        <form name="catwebformform8882" method="post" onsubmit="return checkWholeForm8882(this)" enctype="multipart/form-data" action="/FormProcessv2.aspx?WebFormID=58080&amp;OID=14646047&amp;OTYPE=1&amp;EID=0&amp;CID=0">
    <div style="text-align: center;"><span style="font-size: 18px;">OFFLINE PAYMENT FORM</span></div>
    <div style="text-align: center;"><span style="font-size: 18px;"><br />
    </span></div>
    <div style="text-align: justify;"><span style="font-size: 14px;">Please make sure to attach your payment proof (e.g. photos or scanned copy of your deposit slips, remittance forms, ATM transaction receipts, screenshots of your payment via Online Fund Transfer, screenshots or receipts of your GCash transaction and all applicable documents)</span></div>
    <div style="text-align: justify;"><span style="font-size: 14px;"><br />
    </span></div>
    <div style="text-align: justify;"><span style="font-size: 14px;">Note: Proofs sent in after 4:00 pm, Mondays - Fridays (excluding Holidays and cases of extreme weather) may be processed and confirmed on the next working day. Please expect delays if a huge volume of orders and/or payments are received.</span></div>
    <div style="text-align: left;"><span style="font-size: 18px;"><br />
    </span></div>
    <span class="req">
    <span class="req">*</span>  Required
    <table class="webform" cellspacing="0" cellpadding="2" border="0">
        <tbody>
            <tr>
                <td><label for="Title">Title</label><br />
                <select name="Title" id="Title" class="cat_dropdown_smaller">
                <option value="637365">DR</option>
                <option value="637364">MISS</option>
                <option value="637361" selected="selected">MR</option>
                <option value="637362">MRS</option>
                <option value="637363">MS</option>
                </select></td>
            </tr>
            <tr>
                <td><label for="FirstName">First Name <span class="req">*</span></label><br />
                <input type="text" name="FirstName" id="FirstName" class="cat_textbox" maxlength="255" /> </td>
            </tr>
            <tr>
                <td><label for="LastName">Last Name <span class="req">*</span></label><br />
                <input type="text" name="LastName" id="LastName" class="cat_textbox" maxlength="255" /> </td>
            </tr>
            <tr>
                <td><label for="EmailAddress">Email Address <span class="req">*</span></label><br />
                <input type="text" name="EmailAddress" id="EmailAddress" class="cat_textbox" maxlength="255" /> </td>
            </tr>
            <tr>
                <td><label for="CAT_Custom_470651">Invoice Number <span class="req">*</span></label><br />
                <input type="text" maxlength="255" name="CAT_Custom_470651" id="CAT_Custom_470651" class="cat_textbox" /></td>
            </tr>
            <tr>
                <td><label for="CAT_Custom_349742">Date of Payment <span class="req">*</span></label><br />
                <input type="text" name="CAT_Custom_349742" id="CAT_Custom_349742" class="cat_textbox" readonly="readonly" style="background-color: #f0f0f0;" onfocus="displayDatePicker('CAT_Custom_349742');return false;" /></td>
            </tr>
            <tr>
                <td><label for="CAT_Custom_472580">Bank/Institution Name, if applicable</label><br />
                <input type="text" maxlength="4000" name="CAT_Custom_472580" id="CAT_Custom_472580" class="cat_textbox" /></td>
            </tr>
            <tr>
                <td><label for="CAT_Custom_470654">Payment Method <span class="req">*</span></label><br />
                <select name="CAT_Custom_470654" id="CAT_Custom_470654" class="cat_dropdown">
                <option value=" ">-- Please select --</option>
                <option value="Credit Card">Credit Card</option>
                <option value="Online Fund Transfer/ATM">Online Fund Transfer/ATM</option>
                <option value="Bank Deposit">Bank Deposit</option>
                <option value="Mobile Payment">Mobile Payment</option>
                <option value="Remittance">Remittance</option>
                <option value="Dragon Pay">Dragon Pay</option>
                <option value="e-Gift Certificates">e-Gift Certificates</option>
                </select></td>
            </tr>
            <tr>
                <td><label for="CAT_Custom_470656">Transaction Reference Number <span class="req">*</span></label><br />
                <input type="text" maxlength="4000" name="CAT_Custom_470656" id="CAT_Custom_470656" class="cat_textbox" /></td>
            </tr>
            <tr>
                <td><label for="CAT_Custom_349743">Amount Paid <span class="req">*</span></label><br />
                <input type="text" maxlength="255" name="CAT_Custom_349743" id="CAT_Custom_349743" class="cat_textbox" /></td>
            </tr>
            <tr>
                <td><label for="FileAttachment">Attach File when available (1Mb Limit)</label><br />
                <input type="file" name="FileAttachment" id="FileAttachment" class="cat_textbox" /></td>
            </tr>
            <tr>
                <td><input class="cat_button" type="submit" value="Submit" id="catwebformbutton" /></td>
            </tr>
        </tbody>
    </table>
    <script type="text/javascript" src="http://www.tomato.ph/CatalystScripts/ValidationFunctions.js?vs=b1751.r467413-phase1"></script>
    <script type="text/javascript" src="http://www.tomato.ph/CatalystScripts/Java_DatePicker.js?vs=b1751.r467413-phase1"></script>
    <script type="text/javascript">
//<![CDATA[
var submitcount36109 = 0;function checkWholeForm36109(theForm){var why = "";if (theForm.FirstName) why += isEmpty(theForm.FirstName.value, "First Name");if (theForm.LastName) why += isEmpty(theForm.LastName.value, "Last Name"); if (theForm.EmailAddress) why += checkEmail(theForm.EmailAddress.value); if (theForm.CAT_Custom_470651) why += isCurrency(theForm.CAT_Custom_470651.value, "Invoice Number");if (theForm.CAT_Custom_349742) why += checkDate(theForm.CAT_Custom_349742.value,"Date of Payment");if (theForm.CAT_Custom_470654) why += checkDropdown(theForm.CAT_Custom_470654.value, "Payment Method");if (theForm.CAT_Custom_470656) why += isEmpty(theForm.CAT_Custom_470656.value, "Transaction Reference Number");if (theForm.CAT_Custom_349743) why += isCurrency(theForm.CAT_Custom_349743.value, "Amount Paid");if(why != ""){alert(why);return false;}if(submitcount36109 == 0){submitcount36109++;theForm.submit();return false;}else{alert("Form submission is in progress.");return false;}}
//]]>
</script>
    </span>
</form>

        </div>
        <div style="clear: both;"></div>
        </div>

    </div>
    </div>
    </div>
    </div>
    </div>



    @endsection
