@extends('layouts.master')
@section('content')

<div class="mcont cf">
     <div class="register-wrap">

        <div class="marker-line cf">
            <div class="blue"></div>
            <div class="green"></div>
            <div class="gray"></div>
        </div>

         <div class="panel-title cf">
            <div class="main-page">
                <span>SIGN IN</span>
            </div>
            <div class="description-page">
                Hi folks, welcome to our website as a part of the procedure, creating an account would be awesome.
            </div>
        </div>
             
         <div class="panel-body">

            <!-- <form> -->
            {!! Form::open(['url'=>url('auth/register'), 'class'=>'form-horizontal', 'method'=>'POST', 'id'=>'sender-form', 'onsubmit'=>'','enctype'=>'multipart/form-data']) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                <!--First Name-->
                <div class="form-group">
                    {!!  Form::label('email', 'First Name', array('class' => 'col-md-4 control-label'))!!}
                    
                    <div class="col-md-8">
                        {!! Form::text('first_name',old('first_name'),['','type'=>'text','class'=>'form-control','placeholder' => ''])!!}
                    </div>
                </div>
                
                <!--Middle Name-->
                <div class="form-group">
                    {!!  Form::label('email', 'Middle Name', array('class' => 'col-md-4 control-label'))!!}
                    
                    <div class="col-md-8">
                        {!! Form::text('middle_name',old('middle_name'),['','type'=>'text','class'=>'form-control','placeholder' => ''])!!}
                    </div>
                </div>
                
                <!--Last Name-->
                <div class="form-group">
                    {!!  Form::label('last_name', 'Last Name', array('class' => 'col-md-4 control-label'))!!}
                    
                    <div class="col-md-8">
                        {!! Form::text('last_name',old('last_name'),['','type'=>'text','class'=>'form-control','placeholder' => ''])!!}
                    </div>
                </div>

                <!--Mobile NO. -->
                <div class="form-group">
                    {!!  Form::label('contact_no', 'Mobile No', array('class' => 'col-md-4 control-label'))!!}
                
                    <div class="col-md-8">
                        {!! Form::text('contact_no',old('contact_no'),['','type'=>'text','max-length'=>11,'class'=>'form-control','placeholder' => ''])!!}
                    </div>
                </div>
                
                <!--Birthdate -->
                <div class="form-group">
                    <label class="col-md-4 control-label">
                        Birthday
                    </label> 
                    
                    <div class="col-md-8">
                        <fieldset id="ec-reg-bday" class="bdate">
                            <div class="input-container">
                                <div class="error-msg" id="e-reg-bday"></div>
                                    
                                    <div class="select-style col-xs-4">
                                    {!! Form::selectYear('reg-bday-year', 1950 ,date('Y')-1)!!}
                                    </div>

                                    <div class="select-style col-xs-5">
                                    {!! Form::selectMonth('reg-bday-month')!!}
                                    </div>

                                    <div class="select-style col-xs-3">
                                    {!! Form::selectRange('reg-bday-day', 00, 31)!!}
                                    </div>

                                    <div class="exclamation"></div>
                                <!-- <div class="input-check"></div> -->
                            </div>
                        </fieldset>
                    </div>
                </div>
                
                <!--Email Address -->  
                <div class="form-group">
                    <label class="col-md-4 control-label">E-Mail Add</label>
                    
                    <div class="col-md-8">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                </div>

                <!--Password -->
                <div class="form-group">
                    <label class="col-md-4 control-label">Password</label>
                    
                    <div class="col-md-8">
                            {!! Form::password('password',['','max-length'=>11,'class'=>'form-control'])!!}
                    </div>
                </div>

                <!--Confirm Password -->
                <div class="form-group">
                    <label class="col-md-4 control-label">Confirm Password</label>
                    
                    <div class="col-md-8">
                    {!! Form::password('password_confirmation',['','max-length'=>11,'class'=>'form-control'])!!}
                    </div>
                </div>
                
                <!-- btn submit -->
                <div class="form-group">
                    <div class="col-md-4">
                        <a href="{{url('/auth/login')}}" class="btn-back" style="display:none;">LOG IN</a>
                        <a href="{{url('/auth/login')}}" class="reg_back" style="display:none;">&lt;&lt; back to home</a>
                    </div>
                    <div class="col-md-8">
                        <input type="submit" class="btn-submit" value="REGISTER">
                    </div>
                </div>

            <!-- </form> -->
            {!! Form::close() !!}

        <!-- </panel-body> -->
        </div>
    <!-- </register-wrap> -->
    </div>
<!-- </mcont> -->
</div>

@endsection
