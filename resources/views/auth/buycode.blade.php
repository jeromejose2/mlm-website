    @extends('layouts.master')

    @section('content')

    <div class="container-fluid">
    <div class="row">
    <div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
    <div class="panel-heading">Buy A Code</div>
    <div class="panel-body">

<p>Please input email and password for security purposes</p>

    <form class="form-horizontal" role="form" method="POST" action="/auth/register">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

 <!--    <div class="form-group">
    <label class="col-md-4 control-label">Name</label>
    <div class="col-md-6">
    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
    </div>
    </div> -->

    <div class="form-group">
     <label class="col-md-4 control-label">E-Mail Address</label>
    <div class="col-md-6">
    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
    </div>
    </div>

    <div class="form-group">
    <label class="col-md-4 control-label">Password</label>
    <div class="col-md-6">
    <input type="password" class="form-control" name="password">
    </div>
    </div>

    <div class="form-group">
    <label class="col-md-4 control-label">Confirm Password</label>
    <div class="col-md-6">
    <input type="password" class="form-control" name="password_confirmation">
    </div>
    </div>

    <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
    <button type="submit" class="btn btn-primary">
    Request for Buy Code
    </button>

    </div>
    </div>

    <p>
        Here's the list sample only</p>

        <div style="margin: 0px auto; width: 540px;">
        <p style="text-align: center;"><a style="font-size: 20px; font-weight: bold; text-align: center;" name="q1"><strong><span style="font-family: verdana;">How to Pay via BDO</span></strong></a></p>
        <p style="text-align: left;"><span style="font-family: verdana;">Please take note of our account details.</span></p>
        <p style="text-align: left;"><span style="font-family: verdana; font-size: 16px;"><strong>BDO Account Details:</strong><br />
        Account Name: Primebrands, Inc.<br />
        Savings Account No: 488-005-3905</span></p>
        <h2>
        <p style="text-align: left;"><a name="top"><span style="font-family: verdana;">Click on your preferred payment method for instructions:</span></a></p>
        </h2>
        <div class="faqstyle">
        <ul>
            <li><a href="#q1"><span style="font-family: verdana;">Bank Deposit</span></a></li>
            <li><a href="#q2"><span style="font-family: verdana;">Fund Transfer (Online and Mobile Banking)</span></a></li>
            <li><a href="#q3"><span style="font-family: verdana;">Fund Transfer (BancNet)</span></a></li>
            <li><a href="#q4"><span style="font-family: verdana;">ATM (BancNet, Megalink)</span></a></li>
            <li><a href="#q5"><span style="font-family: verdana;">Enrolling Third Party Account</span></a></li>
        </ul>
        <div><span style="font-family: verdana;">============================================================</span></div>
        </div>
        <div class="ansstyle">
        <h3 class="ansheader">
        <p style="text-align: center; font-size: 20px ! important;"><a name="q1"><span style="font-family: verdana;"><strong style="text-align: center; font-size: 20px ! important;">Bank Deposit</strong><br />
        </span></a></p>
        </h3>
        <p style="text-align: left;"><span style="font-family: verdana;">1. Visit any BDO branch and fill-up a Cash Deposit Slip. </span></p>
        <p style="text-align: center;" sty;e="text-align:center;"><span style="font-family: verdana;"><img alt="" style="border: 0px solid; width: 540px; height: 385px;" src="http://www.tomato.ph/images/2012/Howto/BDO Slip.jpg" />&nbsp;</span></p>
        <p sty;e="text-align:center;"><span style="font-family: verdana;">2.    Pay at the counter. Don&rsquo;t forget your validated receipt!</span></p>
        <p style="text-align: center;" sty;e="text-align:center;"><span style="font-family: verdana;"><img alt="" style="border: 0px solid; width: 500px; height: 338px;" src="http://www.tomato.ph/images/2012/Howto/BDO Guide.jpg" /></span></p>
        <p style="text-align: center;" sty;e="text-align:center;"><span style="font-size: 9px; font-family: verdana;">Photo Credit: Customer submission</span></p>
        <p style="text-align: left;"><span style="font-family: verdana;">3. Submit proof via the <a href="http://www.tomato.ph/offline-payment-form">Offline Payment Form</a>, MMS/Viber to 0918-8TOMATO(8866286) or email to customercare@tomato.ph on or before midnight of the next working day.</span></p>
        <p style="text-align: left;"><span style="font-family: verdana;">Note: BDO charges a transaction fee of P 50.00 when depositing from a provincial location.<br />
        <br />
        <a class="returntop" href="#top">[RETURN TO TOP]</a>
        </span></p>
        </div>
        <span style="font-family: verdana;"><br />
        </span>
        <div class="ansstyle">
        <h3 class="ansheader">
        <p style="text-align: center; font-size: 20px ! important;"><span style="font-family: verdana;"><a name="q2"><strong style="text-align: center; font-size: 20px ! important;"></strong></a><strong style="text-align: center; font-size: 20px ! important;"><a name="bdo-ib">Fund Transfer (Online and Mobile Banking)</a></strong></span></p>
        </h3>
        <p style="text-align: left;"><span style="font-family: verdana;">1. Log in to <a target="_blank" href="https://online.bdo.com.ph/sso/login?josso_back_to=https://online.bdo.com.ph/sso/josso_security_check"><strong>BDO Internet Banking</strong></a> account and select Transfer Funds from the menu.</span></p>
        <p sty;e="text-align:center;">
        </p>
        <div style="text-align: center;"><span style="font-family: verdana;"><img alt="" style="width: 540px; height: 264px;" src="http://www.tomato.ph/images/2012/Howto/BDO EFT/BDO EFT 9.jpg" /></span></div>
        <p style="text-align: left;"><span style="font-family: verdana;">2. Select the Primebrands, Inc. account and input the amount to transfer.</span></p>
        <p sty;e="text-align:center;">
        </p>
        <div style="text-align: center;"><span style="font-family: verdana;"><img alt="" style="width: 540px; height: 328px;" src="http://www.tomato.ph/images/2012/Howto/BDO EFT/BDO EFT 10.jpg" /></span></div>
        <p style="text-align: left;"><span style="font-family: verdana;">3. Confirm payment transfer.</span></p>
        <p sty;e="text-align:center;"><span style="font-family: verdana;"><img alt="" style="border: 0px solid; width: 540px; height: 328px;" src="http://www.tomato.ph/images/2012/Howto/BDO EFT/BDO EFT 11.jpg" /></span></p>
        <p sty;e="text-align:center;"><span style="font-family: verdana;">4.    Take a screenshot of the transaction, or forward the emailed receipt, on or before 12mn of the next working day via email to customercare@tomato.ph, filling up the <a href="http://www.tomato.ph/offline-payment-form">Offline Payment Form</a>, or MMS/Viber to 0918-8TOMATO(8866286).</span></p>
        <p sty;e="text-align:center;"><span style="font-family: verdana;"><img alt="" style="border: 0px solid; width: 540px; height: 365px;" src="http://www.tomato.ph/images/2012/Howto/BDO EFT/BDO EFT Guide.jpg" /><br />
        <br />
        </span></p>
        <p style="text-align: left;"><span style="font-family: verdana;">Need help enrolling the Primebrands, Inc. account? Click <a href="#bdo-enroll">here.<br />
        <br />
        </a><a class="returntop" href="#top">[RETURN TO TOP]</a>
        </span></p>
        </div>
        <span style="font-family: verdana;"><br />
        </span>
        <div class="ansstyle">
        <h3 class="ansheader">
        <p style="text-align: center; font-size: 20px ! important;"><a name="q3"><strong><span style="font-family: verdana;">Fund Transfer (InterBank Fund Transfer)</span></strong></a></p>
        </h3>
        <p style="text-align: left;"><span style="font-family: verdana;">1. Access the BancNet Online website and follow their instructions.</span></p>
        <span style="font-family: verdana;"><br />
        </span>
        <p sty;e="text-align:center;"><span style="font-family: verdana;"><img alt="" style="border: 0px solid; width: 540px; height: 256px;" src="http://www.tomato.ph/images/2012/Howto/BancNet Online.jpg" />
        </span></p>
        <p style="text-align: center; font-size: 9px;"><span style="font-family: verdana;">Screen grab: <a target="_blank" href="https://www.bancnetonline.com/BancnetWeb/goToFundsTransferViaInternetBanking.do">BancNet Online</a></span></p>
        <p style="text-align: left;"><span style="font-family: verdana;">2. Transact payment and take a photo of the transaction receipt. Submit the proof on or before 12mn of the next working day via email to customercare@tomato.ph, filling up the <a href="http://www.tomato.ph/offline-payment-form">Offline Payment Form</a>, or MMS/Viber to 0918-8TOMATO(8866286).</span></p>
        <p style="text-align: center;" sty;e="text-align:center;"><span style="font-family: verdana;"><img alt="" style="border: 0px solid; width: 540px; height: 366px;" src="http://www.tomato.ph/images/2012/Howto/BancNet 1.jpg" />
        </span></p>
        <p style="text-align: center; font-size: 9px;"><span style="font-family: verdana;">Photo Credit: Dragonpay.ph</span></p>
        <span style="font-family: verdana;"><br />
        </span>
        <p style="text-align: left;">
        </p>
        <p style="text-align: left;"><span style="font-family: verdana;">Please note bank fees may apply for InterBank Fund Transfer.</span></p>
        <span style="font-family: verdana;"><br />
        <a class="returntop" href="#top">[RETURN TO TOP]</a>
        </span></div>
        <span style="font-family: verdana;"><br />
        <br />
        </span>
        <div class="ansstyle">
        <h3 class="ansheader">
        <p style="text-align: center; font-size: 20px ! important;"><a name="q4"><strong><span style="font-family: verdana;">ATM</span></strong></a></p>
        </h3>
        <p style="text-align: left;"><span style="font-family: verdana;">1. Visit any BancNet-affiliated ATM and follow the instructions provided. Same instructions also apply to Megalink-affiliated ATM!
        </span></p>
        <p style="text-align: center;" sty;e="text-align:center;"><span style="font-family: verdana;"><img alt="" style="border: 0px solid; width: 540px; height: 227px;" src="http://www.tomato.ph/images/2012/Howto/BancNet ATM.jpg" />
        </span></p>
        <p style="text-align: center; font-size: 9px;"><span style="font-family: verdana;">Screen grab: <a target="_blank" href="https://www.bancnetonline.com/BancnetWeb/goToFundsTransferViaATMPage.do">BancNet Online</a></span></p>
        <p style="text-align: left;"><span style="font-family: verdana;">2. Transact payment and take a photo of the transaction receipt. Submit the proof on or before 12mn of the next working day via email to customercare@tomato.ph, filling up the <a href="http://www.tomato.ph/offline-payment-form">Offline Payment Form</a>, or MMS/Viber to 0918-8TOMATO(8866286).</span></p>
        <p sty;e="text-align:center;"><span style="font-family: verdana;"><img alt="" style="border: 0px solid; width: 540px; height: 439px;" src="http://www.tomato.ph/images/2012/Howto/ATM Guide.jpg" />
        </span></p>
        <p style="text-align: center; font-size: 9px;"><span style="font-family: verdana;">Photo Credit: Customer's submission</span></p>
        <p style="text-align: left;"><span style="font-family: verdana;">Please note bank fees may apply for InterBank ATM Fund Transfer.<br />
        <br />
        <a class="returntop" href="#top">[RETURN TO TOP]</a>
        </span></p>
        </div>
        <div class="ansstyle">
        <h3 class="ansheader">
        <p style="text-align: center; font-size: 20px ! important;"><strong><span style="font-family: verdana;"><a name="q5"></a><a name="bdo-enroll">Enrolling Third Party Accounts</a></span></strong></p>
        <strong>
        </strong></h3>
        <p style="text-align: left;"><span style="font-family: verdana;">1. Log in to your account and select Enroll Other Person&rsquo;s Account from the drop-down menu.</span></p>
        <div style="text-align: center;"><span style="font-family: verdana;"><img alt="" style="border-style: solid; width: 540px; height: 263px;" src="http://www.tomato.ph/images/2012/Howto/BDO EFT/BDO EFT 5.jpg" /></span></div>
        <span style="font-family: verdana;"><br />
        </span>
        <p style="text-align: left;"><span style="font-family: verdana;">2. Input the Primebrands, Inc Account details.</span></p>
        <p sty;e="text-align:center;"><span style="font-family: verdana;"><img alt="" style="border: 0px solid; width: 540px; height: 328px;" src="http://www.tomato.ph/images/2012/Howto/BDO EFT/BDO EFT 6.jpg" /><br />
        <br />
        </span></p>
        <p style="text-align: left;"><span style="font-family: verdana;">3. Confirm the enrollment request.</span></p>
        <p sty;e="text-align:center;"><span style="font-family: verdana;"><img alt="" style="border: 0px solid; width: 540px; height: 328px;" src="http://www.tomato.ph/images/2012/Howto/BDO EFT/BDO EFT 7.jpg" /><br />
        <br />
        </span></p>
        <p style="text-align: left;"><span style="font-family: verdana;">4. Complete the instructions. </span></p>
        <p sty;e="text-align:center;"><span style="font-family: verdana;"><img alt="" style="border: 0px solid; width: 540px; height: 374px;" src="http://www.tomato.ph/images/2012/Howto/BDO EFT/BDO EFT 8.jpg" /></span><span style="font-family: verdana;">Please take note enrollment can potentially take several banking days. Contact your bank for more details.</span></p>
        <p style="text-align: left;"><span style="font-family: verdana;">Congratulations! You have successfully enrolled the Primebrands, Inc. account! You can now pay for your purchases directly online. Simply follow the steps specified in the Fund Transfer (BDO Internet Banking) guide above! <a href="#bdo-ib">Click here</a> </span></p>
        <span style="font-family: verdana;"><br />
        <a class="returntop" href="#top">[RETURN TO TOP]</a>
        </span></div>
        <strong>
        <span style="font-family: verdana;"><br />
        <a href="http://www.tomato.ph/payment-instructions">[Go Back To Previous Page]</a></span>
        </strong></div>

    </form>

    </div>
    </div>
    </div>
    </div>
    </div>



    @endsection
