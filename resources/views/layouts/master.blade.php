<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta name="csrf_token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        <link rel="icon" href="" type="image/x-icon" />

        <!--external-->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">        
        
        <!--not min-->
        <link rel="stylesheet" href="{{asset('assets/css/helpers.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/customBootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/header.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/vendors/fonts/used/style.css')}}">
        <link rel="stylesheet" href="{{asset('assets/vendors/fontawesome/css/font-awesome.min.css')}}">


        <!--main.min.css-->
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    </head>

    <body>
    <header class="main">
        <div class="mcont cf">
            <!-- <a href="" class="capt-logo atag-clear"><h3>FTBI</h3></a> -->
            
            <div class="logo-wrap">
                
                <a href="{{ url('/') }}">
                    <img src="{{ asset('assets/img/logo-wip3.png') }}">
                    <div class="sub-title"><span><i class="fa fa-angle-double-right"></i>&nbsp;BONDS INVESTMENT&nbsp;<i class="fa fa-angle-double-left"></i></span></div>
                </a>
            </div>
            
            <div class="menu-wrap">
                <ul class="cf">
                    <li><a href="">ABOUT US</a></li>
                    <li><a href="">PRODUCTS</a></li>
                    <li><a href="">FAQ</a></li>
                    <li class="cust-line"><a href="">CONTACT US</a></li>
                    <li class="cta-pull-right cust-green"><a href="{{url('/auth/login')}}">LOG IN</a></li>
                    <li class="cta-pull-right cust-blue"><a href="{{url('/auth/register')}}">SIGN IN</a></li>
                </ul>
            </div>
            
        </div>
    </header>

            <!-- Invalid Inputs -->
        	@if (count($errors) > 0)
        	<div class="alert alert-danger">
        	   <div class="mcont cf">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    
                    <strong>Whoops!</strong> There were some problems with your inputs.
                	<ul>
                	     @foreach ($errors->all() as $error)
                		<li>{{ $error }}</li>
                	     @endforeach
                    </ul>
                <!--mcont end-->
                </div>
            </div>
            @endif
            
            <!-- Session Error -->
            @if(session()->has('error'))
            <div class="alert alert-danger" id="alert-error-laravel">
                <div class="mcont cf">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    
                    <span>{{ session('error')['message'] }} </span>
                    {{Session::forget('error')}}
                <!--mcont end-->
                </div>
            </div>

            <!-- Success Login -->
            @elseif(session()->has('success'))
            <div class="alert alert-success" id="alert-success-laravel">
                <div class="mcont cf">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                    <span>{{ session('success')['message'] }} </span>
                    {{Session::forget('success')}}
                <!--mcont end-->
                </div>
            </div>
            @endif

    @yield('content')
    <!--footer-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    

    <script src="{{ asset('assets/js/main.js') }}"></script>
    </body>
</html>
