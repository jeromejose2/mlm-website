    @extends('layouts.master')

    @section('content')



<div class="container">
  


{!! Form::open(['url'=>url('payment/offline-payment-form-post'), 'class'=>'form-horizontal', 'method'=>'POST', 'id'=>'sender-form', 'onsubmit'=>'','enctype'=>'multipart/form-data']) !!}



    <!-- <form class="form-horizontal" role="form" method="POST" action="/auth/login"> -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}">


<!--  <div class="form-group">
{!!  Form::label('refno', 'Reference No.', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6">
{!! Form::text('refno',old('refno'),['','type'=>'text','class'=>'form-control','placeholder' => 'XHJ123'])!!}
    </div>
    </div>
         <td><label for="Title">Title</label><br />
                <select name="Title" id="Title" class="cat_dropdown_smaller">
                <option value="637365">DR</option>
                <option value="637364">MISS</option>
                <option value="637361" selected="selected">MR</option>
                <option value="637362">MRS</option>
                <option value="637363">MS</option>
     -->

    <div style="text-align: center;"><span style="font-size: 18px;">OFFLINE PAYMENT FORM</span></div>
    <div style="text-align: center;"><span style="font-size: 18px;"><br />
    </span></div>
    <div style="text-align: justify;"><span style="font-size: 14px;">Please make sure to attach your payment proof (e.g. photos or scanned copy of your deposit slips, remittance forms, ATM transaction receipts, screenshots of your payment via Online Fund Transfer, screenshots or receipts of your GCash transaction and all applicable documents)</span></div>
    <div style="text-align: justify;"><span style="font-size: 14px;"><br />
    </span></div>
    <div style="text-align: justify;"><span style="font-size: 14px;">Note: Proofs sent in after 4:00 pm, Mondays - Fridays (excluding Holidays and cases of extreme weather) may be processed and confirmed on the next working day. Please expect delays if a huge volume of orders and/or payments are received.</span></div>
    <div style="text-align: left;"><span style="font-size: 18px;"><br />
    </span></div>


<!--  <div class="form-group">
{!!  Form::label('title', 'Title', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6">
{!!
 Form::select('title', array(
 'dr' => 'DR',
 'miss' => 'MISS',
 'mr' => 'MR',
 'mrs' => 'MRS',
 'ms' => 'MS',
  ), 'mr');
!!}
    </div>
    </div>

 <div class="form-group">
{!!  Form::label('first_name', 'First Name', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6">
{!! Form::text('first_name',old('first_name'),['','type'=>'text','class'=>'form-control','placeholder' => 'Juan'])!!}
    </div>
    </div>

     <div class="form-group">
{!!  Form::label('last_name', 'Last Name', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6">
{!! Form::text('last_name',old('last_name'),['','type'=>'text','class'=>'form-control','placeholder' => 'Dela Cruz'])!!}
    </div>
    </div> -->



         <div class="form-group">
{!!  Form::label('invoice_no', 'Invoice Number', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6">
{!! Form::text('invoice_no',old('invoice_no'),['','type'=>'text','maxlength'=>'10','class'=>'form-control','placeholder' => '123456'])!!}
    </div>
    </div>





         <div class="form-group">
{!!  Form::label('date_payment', 'Date of Payment', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6">
{!! Form::text('date_payment',old('date_payment'),['','type'=>'text','maxlength'=>'10','class'=>'form-control','placeholder' => 'Mm-dd-yyyy'])!!}
    </div>
    </div>

             <div class="form-group">
{!!  Form::label('bank', 'Bank/Institution Name, if applicable', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6">
{!! Form::text('bank',old('bank'),['','type'=>'text','maxlength'=>'10','class'=>'form-control','placeholder' => ''])!!}
    </div>
    </div>


 <div class="form-group">
{!!  Form::label('payment_method', 'Payment Method', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6">
{!!
 Form::select('payment_method', array(
 'ps' => 'Please select',
 'creditcard' => 'Credit Card',
 'bankdeposit' => 'Bank Deposit',
 'mobilepayment' => 'Mobile Payment',
 'dragonpay' => 'Dragon Pay',
 'paypal' => 'Pay Pal',
  ), 'ps');
!!}
    </div>
    </div>

 <div class="form-group">
{!!  Form::label('transaction_ref_no', 'Transaction Reference Number', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6">
{!! Form::text('transaction_ref_no',old('transaction_ref_no'),['','type'=>'text','maxlength'=>'10','class'=>'form-control','placeholder' => '123345'])!!}
    </div>
    </div>

             <div class="form-group">
{!!  Form::label('amount_paid', 'Amount Paid', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6 input-group">
     <div class="input-group-addon">PHP</div>
{!! Form::text('amount_paid',old('amount_paid'),['','type'=>'text','maxlength'=>'10','class'=>'form-control','placeholder' => '5000'])!!}
    <div class="input-group-addon">.00</div>
    </div>
    </div>




     <div class="form-group">
{!!  Form::label('image', 'Attach File when available (1Mb Limit)', array('class' => 'col-md-4 control-label')) !!}
    <div class="col-md-6">
{!! Form::file('image'); !!}
    </div>
    </div>


 <div class="form-group">
{!!  Form::label('sponsor_id', 'FOR Sponsor Information', array('class' => 'col-md-4 control-label'))!!}
    </div>

 <div class="form-group">
{!!  Form::label('sponsor_id', 'Sponsor ID (buycodeID)', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6">
{!! Form::text('sponsor_id',old('sponsor_id'),['','maxlength'=>'10','type'=>'text','class'=>'form-control','placeholder' => '123xxxx'])!!}
    </div>
    </div>

 <div class="form-group">
{!!  Form::label('upline_id', 'Upline ID (buycodeID)', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6">
{!! Form::text('upline_id',old('upline_id'),['','maxlength'=>'10','type'=>'text','class'=>'form-control','placeholder' => '123xxx'])!!}
    </div>
    </div>

     <div class="form-group">
{!!  Form::label('position', 'Position', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6">
{!!  Form::select('position', array(' ','left' => 'Left', 'S' => 'Right')) !!}
    </div>
    </div>

     <div class="form-group">
{!!  Form::label('package', 'Package', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6">
{!!  Form::select('package', array(' ','regular' => 'Regular (2,000)', 'premium' => 'Premium (5,000)','gold'=>'Gold (10,000)')) !!}
    </div>
    </div>

         <div class="form-group">
{!!  Form::label('email', 'Email Address', array('class' => 'col-md-4 control-label'))!!}
    <div class="col-md-6">
{!! Form::text('email',old('email'),['','type'=>'text','class'=>'form-control','placeholder' => 'juan@delacruz.com'])!!}
    </div>
    </div>

    <div class="form-group">
    <label class="col-md-4 control-label">User Password</label>
    <div class="col-md-6">
    <input type="password" class="form-control" name="password">
    </div>
    </div>

    <div class="form-group">
    <label class="col-md-4 control-label">Confirm Password</label>
    <div class="col-md-6">
    <input type="password" class="form-control" name="password_confirmation">
    </div>
    </div>

    
    <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
    
{!! Form::submit('Submit', array('class'=>'btn btn-primary')) !!}

    </div>
    </div>
    <!-- </form> -->
    {!! Form::close() !!}

</div>



    @endsection
