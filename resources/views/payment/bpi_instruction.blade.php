    @extends('layouts.master')

    @section('content')

    bpi_instruction.blade.php

<div class="container">
  
        <div style="margin: 0px auto; width: 540px;">
        <p style="text-align: center;"><a style="font-size: 20px; font-weight: bold; text-align: center;" name="q1"><strong><span style="font-family: verdana;">How to Pay via BPI</span></strong></a></p>
        <p style="text-align: left;"><span style="font-family: verdana;">Please take note of our account details.</span></p>
        <p style="text-align: left;"><span style="font-family: verdana; font-size: 16px;"><strong>BPI Account Details:</strong><br />
        Account Name: Primebrands, Inc.<br />
        Savings Account No: 1881-0462-41</span></p>
      <!--   <h2>
        <p style="text-align: left;"><a name="top"><span style="font-family: verdana;">Click on your preferred payment method for instructions:</span></a></p>
        </h2> -->
        <div class="faqstyle">
       <!--  <ul>
            <li><a href="#q1"><span style="font-family: verdana;">Bank Deposit</span></a></li>
            <li><a href="#q2"><span style="font-family: verdana;">Fund Transfer (Online and Mobile Banking)</span></a></li>
            <li><a href="#q3"><span style="font-family: verdana;">Fund Transfer (BancNet)</span></a></li>
            <li><a href="#q4"><span style="font-family: verdana;">ATM (BancNet, Megalink)</span></a></li>
            <li><a href="#q5"><span style="font-family: verdana;">Enrolling Third Party Account</span></a></li>
        </ul> -->
        <div><span style="font-family: verdana;">============================================================</span></div>
        </div>
        <div class="ansstyle">
        <h3 class="ansheader">
        <p style="text-align: center; font-size: 20px ! important;"><a name="q1"><span style="font-family: verdana;"><strong style="text-align: center; font-size: 20px ! important;">Bank Deposit</strong><br />
        </span></a></p>
        </h3>
        <p style="text-align: left;"><span style="font-family: verdana;">1. Visit any BPI branch and fill-up a Deposit/Payment Slip. </span></p>
        <p style="text-align: center;" sty;e="text-align:center;"><span style="font-family: verdana;"><img alt="" style="border: 0px solid; width: 540px; height: 272px;" src="http://www.tomato.ph/images/2012/Howto/BPI Slip.jpg" /></span></p>
        <p sty;e="text-align:center;"><span style="font-family: verdana;">2.    Pay at the counter. Don&rsquo;t forget your validated receipt!</span></p>
        <p style="text-align: center;" sty;e="text-align:center;"><span style="font-family: verdana;"><img alt="" style="border: 0px solid; width: 540px; height: 498px;" src="http://www.tomato.ph/images/2012/Howto/BPI Guide.jpg" /></span></p>
        <p style="text-align: center;" sty;e="text-align:center;"><span style="font-size: 9px; font-family: verdana;">Photo Credit: Customer submission</span></p>
        <p style="text-align: left;"><span style="font-family: verdana;">3. Submit proof via the 
            <a href="{{url('/payment/offline-payment-form')}}">Offline Payment Form</a>, MMS/Viber to 0918-8TOMATO(8866286) or email to customercare@tomato.ph on or before midnight of the next working day.</span></p>
        <p style="text-align: left;"><span style="font-family: verdana;"><br />
        <a class="returntop" href="#top">[RETURN TO TOP]</a>
        </span></p>
        </div>
        <span style="font-family: verdana;"><br />
        </span>

</div>




    @endsection
