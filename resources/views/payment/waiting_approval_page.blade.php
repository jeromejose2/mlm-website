@extends('layouts.master')

@section('content')
	<div class="mcont">
		
		<div class="waiting-approval cf">
			<div class="marker-line cf">
	            <div class="blue"></div>
	            <div class="green"></div>
	            <div class="gray"></div>
        	</div>
			
        	<div class="message">
        		<span class="title big-font label-blue">Oops! :D</span>
				<br/>
				<br/>
        		<p>Your payment is still under process for verification, kindly visit this page not more than 2-3 days.</p>
        	</div>

        	<div class="status">
        		<span class="label-bold">Status:&nbsp;&nbsp;</span><span class="label-green">Ongoing</span>
        		<br/>
        		<span class="label-bold">Package:&nbsp;&nbsp;</span><span class="label-green">Regular(2k)</span>
        	</div>
			
			<br/>
			<span class="label-red">*NOTE: Status can be Pending, Ongoing or Refused</span>
			<br/>
			<br/>
        	<a href="" class="btn-default btn-alert">CANCEL</a> 
		</div>

	</div>
@endsection