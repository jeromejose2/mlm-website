@extends('layouts.master')
@section('content')

<div class="mcont cf">
    <div class="payment-landing-wrap">
        
        <div class="marker-line cf">
            <div class="blue"></div>
            <div class="green"></div>
            <div class="gray"></div>
        </div>

        <div class="panel-title align-left cf">

            <div class="main-page">
                <span class="add-buycode">ADD BUYCODE</span>
            </div>

            <div class="description-page">
                Already have a BUYCODE ID? If YES, you can now add your BUYCODE ID together with your settled package to activate your account.
            </div>

        </div>
             
        <div class="panel-body">
        {!! Form::open(['url'=>url('payment/register'), 'class'=>'form-horizontal', 'method'=>'POST', 'id'=>'payment-form', 'onsubmit'=>'','enctype'=>'multipart/form-data']) !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        
        <!--/sponsor_id-->
        <div class="form-group">
            {!!  Form::label('sponsor_id', 'Sponsor ID (buycodeID)', array('class' => 'col-md-4 control-label'))!!}
        
            <div class="col-md-8">
                {!! Form::text('sponsor_id',old('sponsor_id'),['','type'=>'text','maxlength'=>'10','class'=>'form-control','placeholder' => ''])!!}
            </div>
        </div>
        
        <!--/upline_id-->
       <div class="form-group">
            {!!  Form::label('upline_id', 'Upline ID (buycodeID)', array('class' => 'col-md-4 control-label'))!!}
            
            <div class="col-md-8">
                {!! Form::text('upline_id',old('upline_id'),['','maxlength'=>'10','type'=>'text','class'=>'form-control','placeholder' => ''])!!}
            </div>
        </div>
    
        <!--/position-->
            <div class="form-group">
            {!!  Form::label('position', 'Position', array('class' => 'col-md-4 control-label'))!!}
            
            <div class="col-md-8">
                <div class="select-style col-xs-6">
                    {!!  Form::select('position', array('---','left' => 'Left', 'S' => 'Right')) !!}
                </div>
            </div>
        </div>

        <!--/package-->
        <div class="form-group">
            {!!  Form::label('package', 'Package', array('class' => 'col-md-4 control-label'))!!}
            
            <div class="col-md-8">
                <div class="select-style col-xs-6">
                    {!!  Form::select('package', array('---','regular' => 'Regular (2K)', 'premium' => 'Premium (8K)','gold'=>'Gold (36K)')) !!}
                </div>
            </div>
        </div>

        <!--/password-->
        <div class="form-group">
            <label class="col-md-4 control-label">Current Password</label>
            
            <div class="col-md-8">
                <input type="password" class="form-control" name="password">
            </div>
        </div>

        <!--/confirm password-->
        <div class="form-group">
            <label class="col-md-4 control-label">Confirm Password</label>
            
            <div class="col-md-8">
                <input type="password" class="form-control" name="password_confirmation">
            </div>
        </div>

        <!--/submit-->
        <div class="form-group">
            <div class="col-md-4">
                <!--<a href="{{url('/auth/login')}}" class="btn-back">Back</a>-->
            </div>
            
            <div class="col-md-8">
                {!! Form::submit('ACTIVATE', array('class'=>'btn-submit')) !!}
                {!! Form::button('sample ajax', array('class'=>'send-btn123')) !!}
            </div>
        </div>

        <!--/form-->
        {!! Form::close() !!}

        <!--/panel-body-->
        </div>




        <!-- DRAGON PAY PAYMENT -->
        <div class="panel-title force-align-left cust-margin">
            <p style="color: red; font-family:amblelight_condensed;">* NOTE: If you don't have a BUYCODE ID, you can choose a mode of payment listed below (Dragon Pay, Paypal, Bank Deposit)</p>
        </div>

        

        <!-- DRAGON PAY PAYMENT -->
        <div class="line-gap cf"></div>
        <div class="panel-title align-left cf">
            <div class="main-page">
                <span class="add-buycode">DRAGON PAY</span>
            </div>

            <div class="description-page">
                You can purchase BUYCODE ID through Dragon Pay. More information added here maintain atleast two lines or more.
            </div>
        </div>

        <div class="panel-body cf">
            <div class="left-wrap">&nbsp;</div>
            
            <div class="right-wrap">
                <a href="" class="btn-dragonpay">
                    <img src="{{ asset('assets/img/dragonpay.png') }}">
                </a>
            </div>
        </div>

        <!-- PAYPAL PAYMENT -->
        <div class="line-gap cf"></div>
        <div class="panel-title align-left cf">
            <div class="main-page">
                <span class="add-buycode">PAYPAL</span>
            </div>

            <div class="description-page">
                Only email payment through paypal, not include credit cards. More information added here maintain atleast two lines or more.
            </div>
        </div>

        <div class="panel-body cf">
            <div class="left-wrap">&nbsp;</div>
            
            <div class="right-wrap">
                <a href="" class="btn-dragonpay">
                    <img src="{{ asset('assets/img/paypal.png') }}">
                </a>
            </div>
        </div>





        

        <!-- BANK DEPOSIT PAYMENT -->
        <div class="line-gap cf"></div>
        <div class="panel-title align-left cf">

            <div class="main-page">
                <span class="add-buycode">BANK DEPOSIT</span>
            </div>

            <div class="description-page">
                Direct payment through Bank deposit here. More information added here maintain atleast two lines or more.
            </div>

        </div>

        <div class="panel-body cf">
            <div class="left-wrap">&nbsp;</div>
            
            <div class="right-wrap">
                <div class="amble-condense">* You can put information here either two or more lines.</div>
                <a class="via-bpi" href="{{url('payment/how-to-pay-via-bpi')}}">
                    <img src="{{ asset('assets/img/via-bpi.png')}}" alt="">
                </a>
                
                <div class="amble-condense">* You can put information here either two or more lines.</div>
                <a class="via-bdo" href="{{url('payment/how-to-pay-via-bdo')}}">
                    <img src="{{ asset('assets/img/via-bdo.png')}}" alt="">
                </a>
            </div>
        </div>

    <!--/payment-landing-wrap-->
    </div>
<!--/mcont-->
</div>

<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script>


<script type="text/javascript">
$(document).ready(function(){

    $filters = $('#payment-form');
    $filters.on('change', '#sponsor_id', function() 
    {  get_sponsor('sponsor');    });

        $filters.on('change', '#upline_id', function() 
    {  get_sponsor('upline');    });

});

function get_sponsor(type)
{
      $.ajax({
                      url: '{{url("ajax/getsponsor")}}',
                      type: "post",
                      data: {'sponsor_id':$('input[name=sponsor_id]').val(), '_token': $('input[name=_token]').val()},
                      success: function(data){
                      
                        if(data == '0')
                        {
                            if(type == 'upline'){ alert('Please input existing upline ID '); $('input[name=upline_id]').val(''); }
                            if(type == 'sponsor'){ alert('Please input existing sponsor ID '); $('input[name=sponsor_id]').val(''); }
                           
                        }
                      }
                    }); 
}
</script>

@endsection
