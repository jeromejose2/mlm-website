@extends('layouts.master')
@section('content')


<!-- {{ var_dump(!isset($user->buycode_start_time)) }} -->
<div class="mcont cf">
    @if($paid)
        <!-- paid and approved -->
        @if($approved)
        <div class="form-group">
            <div class="col-md-6">
                Buy CODE ID : {{ $user->active_buycode_id }}
            </div>
        </div>

            @if(!isset($user->buycode_start_time) && $user->buycode_start_time == '')
                <div class="form-group">
                    Click this to start the countdown
                    <div class="col-md-6">
                        {!!Form::submit('Start 60 Days!'); !!}
                        <a href="{{url('/start_the_countdown')}}" class="btn btn-error">Start 60 Days! {{url('/start_the_countdown')}}</a>
                    </div>
                </div>
                
                <a href="{{url('/payment')}}" class="btn btn-error">Payment </a>
            
            @else
                <div class="form-group">
                Your time is 
                    <div class="col-md-6">
                        Start: {{ $user->buycode_start_time }}
                        End: {{ $user->buycode_end_time }}
                    <br><br>
                    <!-- <a href="{{url('/start_the_countdown')}}" class="btn btn-error">Start 60 Days! {{url('/start_the_countdown')}}</a> -->
                    <a href="{{url('/geneology')}}" class="btn btn-error">Geneology</a>
                    <a href="{{url('/payment')}}" class="btn btn-error">Payment </a>
                    </div>
                </div>
            @endif

        <!-- pending approval -->
        @else
            please wait for the admin to verify ur payment
        @endif
    
    <!-- not paid -->
    @else
        <!--if no records of payment go to this page-->
        <!--<a href="{{url('/payment')}}" class="btn btn-error">Payment </a>-->

        <div class="no-record cf">
            <div class="marker-line cf">
                <div class="blue"></div>
                <div class="green"></div>
                <div class="gray"></div>
            </div>
            
            <div class="message">
                <span class="title big-font label-blue">Hi, </span>
                <br/>
                <p>As part of website process you need to get buycode id first. If no records of payment, more message should be added here.</p>
                <br/>
            </div>
            <a href="{{url('/payment')}}" class="btn-default btn-prim">Get Buycode ID</a>
        </div>
    @endif

</div>

@endsection