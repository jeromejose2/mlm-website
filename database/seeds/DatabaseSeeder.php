<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\MembersInfo;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         // $this->call('UserTableSeeder');

        DB::table('users')->truncate();

        User::create([
            'name' => 'We got your message!',
            'email' => 'Product Related',
            'password' => '123123.'
        ]);

   DB::table('members_info')->truncate();

        MembersInfo::create([
            'email' => 'jerome.jose@nuworks.ph',
            'password' => '123456',
            'first_name' => 'Jerome',
            'last_name' => 'Jose'
        ]);

$this->command->info('All tables are seeded!');
       // Model::reguard();
    }
}
