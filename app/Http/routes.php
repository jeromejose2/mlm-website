<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/**Login **/
Route::controllers( [
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
	] );

/** DragonPAy **/
Route::get( '/returnurl', 'PaymentController@returnurl' ); //after entering the dragonpay
Route::get( '/postback', 'PaymentController@postback' ); //after Success PAYMENT in Dragonpay
Route::post( '/payment/register', 'PaymentController@register' );




/** Payment **/
Route::get( '/payment', 'PaymentController@index' );
Route::get( '/payment/how-to-pay-via-bdo', 'PaymentController@bdo_instruction' );
Route::get( '/payment/how-to-pay-via-bpi', 'PaymentController@bpi_instruction' );
Route::get( '/payment/offline-payment-form', 'PaymentController@offline_payment_form' );
Route::post( '/payment/offline-payment-form-post', 'PaymentController@offline_payment_form_post' );
Route::post( '/payment/register', 'PaymentController@register' );



/** ajax **/
Route::post('ajax/getsponsor', 'DashboardController@ajax_get_sponsor_id');



/** Dashboard **/
Route::get( '/firstdashboard', 'DashboardController@index' );
Route::get( '/start_the_countdown', 'DashboardController@startcountdown' );


/** Geneology **/
Route::get( '/geneology', 'DashboardController@geneology' );

/* Middleware */
Route::group( ['middleware'=>'auth'], function() {

		/* Dashboard */
		Route::get( '/', 'WelcomeController@index' );





		/* Logout */
		Route::get( 'logout',  'WelcomeController@logOut' );

	} );


/** Dummy Pages might be these pages(Waiting Approval) **/
Route::get('/payment/dummy/wait-approval', 'DummyController@wait_approval');
