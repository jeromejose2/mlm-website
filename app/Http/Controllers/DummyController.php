<?php 

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\User;
use App\MembersInfo;
use App\MembersDownline;
use App\MembersTransaction;
use Validator;
use DB;
use Input;
use Request;
use Redirect;
use Mail;

class DummyController extends Controller {
	
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('welcome');
	}

	public function wait_approval(){
		return view('payment.waiting_approval_page');
	}
}