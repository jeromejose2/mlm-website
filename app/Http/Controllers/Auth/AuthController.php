<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\User;
use App\MembersInfo;
use App\MembersDownline;
use App\MembersTransaction;
use Validator;
use DB;
use Input;
use Request;
use Redirect;
use Mail;
class AuthController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, $this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    //use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    use AuthenticatesAndRegistersUsers;
    protected $redirectPath = '/dashboard';
    protected $loginPath = '/login';
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    // public function __construct() {
    //     $this->middleware( 'guest', ['except' => 'getLogout'] );
    // }

    public function __construct( Guard $auth, Registrar $registrar ) {
        $this->auth = $auth;
        $this->registrar = $registrar;

        $this->middleware( 'guest', ['except' => 'getLogout'] );
    }

    /**
     * Get a $validator for an incoming registration request.
     *
     * @param array   $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data ) {
        return Validator::make( $data, [
            'first_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            ] );
    }

    protected function validator_login( array $data ) {
        return Validator::make( $data, [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            ] );
    }
    /**
     * Create a new $user instance after a valid registration.
     *
     * @param array   $data
     * @return User
     */
    protected function create( array $data ) {
        return User::create( [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt( $data['password'] ),
            ] );
    }
    public function getRegister() {

        return view( 'auth.register' );
    }

    public function postRegister() {

        $validator = $this->validator( Request::all() );

        $message_fail = " fail ";
        $message_success = " success";
        if ( $validator->fails() ) {
            return Redirect::back()
            ->withMessage( $message_success )->withErrors( $validator )->withInput( Input::except( 'password' ) );

        }else {

            /*check email*/
            $user = DB::table( 'members_info' )->where( 'email', Request::input( 'email' ) )->count();
            if ( $user != 0 ) {
                session()->put( 'error', [
                    'message'=>'Email taken.Please try again.'
                    ] );
                return Redirect::back()->withInput( Input::except( 'password' ) );
            }
            /*check email*/



            $Category = new MembersInfo;
            $Category->email = Request::input( 'email' );
            $Category->password = md5( trim( Request::input( 'password' ) ) );
            $Category->first_name = Request::input( 'first_name' );
            $Category->middle_name = Request::input( 'middle_name' );
            $Category->last_name = Request::input( 'last_name' );
            $Category->contact_no = Request::input( 'contact_no' );
            $Category->birth_date = Request::input( 'reg-bday-year' ).'-'.Request::input( 'reg-bday-month' ).'-'.Request::input( 'reg-bday-day' );
            $imageName = '';
            // if (Request::hasFile('image'))
            // {
            //     if (Request::file('image')->isValid())
            //         {

            //             $imageName = '-product-' .Input::file('image')->getClientOriginalName();
            //             $move =   Request::file('image')->move(base_path() . '/public/uploads/products/', $imageName);
            //             $Category->image = 'products/'.$imageName;
            //         }
            // }
            $save = $Category->save();
            session()->put( 'success', [
                'message'=>'Added Successfully.Please Login'
                ] );
            return redirect( 'auth/login' );


        }

    }

    public function getLogin() {
        return view( 'auth.login' );
    }

    public function postLogin() {

        $validator = $this->validator_login( Request::all() );

        $message_fail = " fail ";
        $message_success = " success";
        if ( $validator->fails() ) {
            return Redirect::back()->withInput( Input::except( 'password' ) )->withErrors( $validator );
        }else {




            $user = DB::table( 'members_info' )->where( 'email', Request::input( 'email' ) )->where( 'password', md5( trim( Request::input( 'password' ) ) ) )->first();
            // $user = DB::table( 'members_info' )->where( 'email', Request::input( 'email' ) )->first();
            if ( $user ) {
                session()->put( 'user', [
                    'id'=>$user->member_id,
                    'firstname'=>$user->first_name,
                    'lastname'=>$user->last_name,
                    'email'=>$user->email
                    ] );

                session()->put( 'success', [
                    'message'=>'Session add Successfully.'
                    ] );
                 return redirect( 'firstdashboard' );
            }else {
                session()->put( 'error', [
                    'message'=>'Session Error.'
                    ] );
                return redirect('auth/login');
            }

           

        }
    }


}
