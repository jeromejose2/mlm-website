<?php


namespace App\Http\Controllers;

//use Illuminate\Http\Request;

// use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\MembersInfo;
use App\MembersDownline;
use App\MembersTransaction;
use Validator;
use DB;
use Input;
use Request;
use Redirect;
use Mail;
class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * 09285598971/09052111604/09215147600
     *
     * @return Response
     */


 public function __construct()
    {

          $member_id = session('user')['id'];
          if(!$member_id)
            { return redirect('auth/login');}
    }

      public function ajax_get_sponsor_id() {
    // Getting all post data
    if(Request::ajax()) {
      $data = Input::all();
     
        $sponsor = DB::table( 'members_info' )->where( 'active_buycode_id', $data['sponsor_id'] )->first();
        if(!$sponsor)
        {
            return 0;
        }else
        {
            return $data;
        }
    }
}


    public function index() {
        $member_id = session('user')['id'];

        $user = DB::table( 'members_info' )->where( 'member_id', $member_id )->first();
    
        $countPaid_payment = DB::table( 'members_downline' )->where( 'member_id', $member_id )->where( 'is_paid', '1' )->get();


        $get_approved = DB::table( 'approved_log' )->where( 'member_id', $member_id )->OrderBy( 'id', 'desc' )->first();//get the latest
        $get_transaction = DB::table( 'members_transaction' )->where( 'member_id', $member_id )->OrderBy( 'id', 'desc' )->first();//get the latest
        $admin_approved = false; //kapag nasa approved log means na approve na ni admin
        if ( $get_approved ) {
            $admin_approved = true;
            if ( $get_approved->ref_no != $get_transaction->ref_no && $get_approved->transaction_no != $get_transaction->transaction_no ) {
                echo 'transaction and approved not tally';die();
            }
        }


        $paid = false;
        if ( $countPaid_payment ) {
            $paid = true;
        }

        if ( $user ) {

            if ( $user->active_buycode_id && $user->buycode_start_time == '' ) //approved by admin and not start the time
                {

                return view( 'dashboard.index' )->withUser( $user )->withPaid( $paid )->withApproved( $admin_approved );


            }else {
                return view( 'dashboard.index' )->withUser( $user )->withPaid( $paid )->withApproved( $admin_approved );
            }


        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function startcountdown() {
        $member_id = session( 'user' )['id'];
        $user = DB::table( 'members_info' )->where( 'member_id', $member_id )->first();
        if ( $user ) {
            $start_date = date( 'Y-m-d H:m:s' );
            $end_date = date( 'Y-m-d H:m:s', strtotime( "+60 days" ) );
            $param = array();
            $param['buycode_start_time'] = $start_date;
            $param['buycode_end_time'] = $end_date;


            $boo =  DB::table( 'members_info' )->where( 'member_id', $member_id )->update( $param );
            session()->put( 'success', [
                'message'=>'Timer Starts NOw.'
                ] );
            return view( 'dashboard.index' )->withUser( $user );
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function geneology() {

        $member_id = session( 'user' )['id'];
        $user = DB::table( 'members_info' )->where( 'member_id', $member_id )->first();
        $data_array = array();
        if ( $user ) {
            $main_user = DB::table( 'members_downline' )->where( 'active_buycode_id', $user->active_buycode_id )->get();
            foreach ( $main_user as $key => $value ) {
                $data_array = array( array(
                        'key'=> $value->active_buycode_id,
                        'name'=> $user->first_name,
                        'gender'=> "M",
                        'birthYear'=> "1865",
                        'deathYear'=> "1865",
                        'reign'=> "1910-1936"
                    ) );
                $downline_1 = DB::table( 'members_downline' )->where( 'upline_buycode_id', $value->active_buycode_id )->join( 'members_info', 'members_downline.member_id', '=', 'members_info.member_id' )->get();

                foreach ( $downline_1 as $key => $value_1 ) {

                    array_push( $data_array, array(
                            'key'=> $value_1->active_buycode_id,
                            'parent'=> $value_1->upline_buycode_id,
                            'name'=> $value_1->first_name,
                            'gender'=> "M",
                            'birthYear'=> "1865",
                            'deathYear'=> "1865",
                            'reign'=> "1910-1936"
                        ) );

                $downline_2 = DB::table( 'members_downline' )->where( 'upline_buycode_id', $value_1->active_buycode_id )->join( 'members_info', 'members_downline.member_id', '=', 'members_info.member_id' )->get();
                          foreach ( $downline_2 as $key => $value_2 ) 
                          {
                             array_push( $data_array, array(
                            'key'=> $value_2->active_buycode_id,
                            'parent'=> $value_2->upline_buycode_id,
                            'name'=> $value_2->first_name,
                            'gender'=> "M",
                            'birthYear'=> "1865",
                            'deathYear'=> "1865",
                            'reign'=> "1910-1936"
                                ) );

                            

                                    $downline_3 = DB::table( 'members_downline' )->where( 'upline_buycode_id', $value_2->active_buycode_id )->join( 'members_info', 'members_downline.member_id', '=', 'members_info.member_id' )->get();
                                  foreach ( $downline_3 as $key => $value_3 ) 
                                  {

                                           array_push( $data_array, array(
                                                'key'=> $value_3->active_buycode_id,
                                                'parent'=> $value_3->upline_buycode_id,
                                                'name'=> $value_3->first_name,
                                                'gender'=> "M",
                                                'birthYear'=> "1865",
                                                'deathYear'=> "1865",
                                                'reign'=> "1910-1936"
                                                    ) );


                                                   $downline_4 = DB::table( 'members_downline' )->where( 'upline_buycode_id', $value_3->active_buycode_id )->join( 'members_info', 'members_downline.member_id', '=', 'members_info.member_id' )->get();
                                                              foreach ( $downline_4 as $key => $value_4 ) 
                                                              {
                                                                  array_push( $data_array, array(
                                                                    'key'=> $value_4->active_buycode_id,
                                                                    'parent'=> $value_4->upline_buycode_id,
                                                                    'name'=> $value_4->first_name,
                                                                    'gender'=> "M",
                                                                    'birthYear'=> "1865",
                                                                    'deathYear'=> "1865",
                                                                    'reign'=> "1910-1936"
                                                                        ) );
                                                              }
                                  }
                          }



                }

                $value = '';$key = '';
                for ( $i=0; $i < count( $data_array ); $i++ ) { $new_arr[] = $data_array[$i];       }
                return view( 'dashboard.tree' )->withTree( json_encode( $new_arr, JSON_PRETTY_PRINT ) );


            }
            $downline = DB::table( 'members_downline' )->where( 'sponsor_buycode_id', $user->active_buycode_id )->join( 'members_info', 'members_downline.member_id', '=', 'members_info.member_id' )->get();

            foreach ( $downline as $key => $value ) {
                // code...
                print_r( '<pre>' );
                var_dump( $value );

                print_r( '</pre>' );die();
                $downline1 = DB::table( 'members_downline' )->where( 'active_buycode_id', $downline->active_buycode_id )->join( 'members_info', 'members_downline.member_id', '=', 'members_info.member_id' )->get();

            }

            return view( 'dashboard.tree' );

            // return view( 'dashboard.geneology' )->withDownline($downline)->withUser($user);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int     $id
     * @return Response
     */
    public function show( $id ) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int     $id
     * @return Response
     */
    public function edit( $id ) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     * @return Response
     */
    public function update( Request $request, $id ) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int     $id
     * @return Response
     */
    public function destroy( $id ) {
        //
    }
}
