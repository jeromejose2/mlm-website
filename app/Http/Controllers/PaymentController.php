<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

// use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\MembersInfo;
use App\MembersDownline;
use App\MembersTransaction;
use Validator;
use DB;
use Input;
use Request;
use Redirect;
use Mail;
define( 'DRAGONPAY_TEST_URL',                'http://test.dragonpay.ph/Pay.aspx' );
define( 'DRAGONPAY_PRODUCTION_URL',          'https://gw.dragonpay.ph/Pay.aspx' );
define( 'DRAGONPAY_TEST_USER',               'NUWORKS' );
define( 'DRAGONPAY_TEST_PASSWORD',           'G9rE3PyT4' );
define( 'DRAGONPAY_PRODUCTION_USER',         'EPIC' );
// define( 'DRAGONPAY_PRODUCTION_PASSWORD',     'V8rQE93w' );
 define( 'DRAGONPAY_PRODUCTION_PASSWORD',     '123123123' );


define( 'DRAGONPAY_PRODUCTION_API_URL',      'https://gw.dragonpay.ph/DragonPayWebService/MerchantService.asmx' );
define( 'DRAGONPAY_TEST_API_URL',        'http://test.dragonpay.ph/DragonPayWebService/MerchantService.asmx' );

class PaymentController extends Controller
{



 public function __construct()
    {

          $member_id = session('user')['id'];
          if(!$member_id)
            { return redirect('auth/login');}
    }
    /**
     * HOW IT WORKS
     * 1. login 2. payment/register.
     * 3. DP will redirect to payment/return url....return url sample : http://www.nestea.com.ph/index.php/returnurl?txnid=9f593a&refno=84ZQAFV0&status=P&message=%5b000%5d+Waiting+for+deposit+to+BogusBank+OTC+%2384ZQAFV0&digest=2b2ae9c448696fbb3016d14f30a8c4375b295934
     * 4  DP will redirect to succesurl .success payemnt go to payment/postback THATS ALL
     * 5. FINANCE ADMIN WILL CHECK or APPROVE then it will generate buycode for the member
     */


    protected function validator_payment( array $data ) {
        return Validator::make( $data, [
             'sponsor_id' => 'required',
             'upline_id' => 'required',
             'package' => 'required',
             'position' => 'required',
             'password' => 'required|confirmed|min:6',
            ] );
    }

       /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return view( 'payment.index' );
    }

      public function bdo_instruction() {
        return view( 'payment.bdo_instruction' );
    }

  public function bpi_instruction() {
        return view( 'payment.bpi_instruction' );
    }

     public function offline_payment_form() {


        return view( 'payment.offline_payment_form' );
    }
     public function offline_payment_form_post() {

          $member_id = session( 'user' )['id'];
            $rules = array(
             'email' => 'required|email',
             'invoice_no' => 'required|numeric',
             'date_payment' => 'required|date',
             'bank' => 'required|string',
             'payment_method' => 'required|string',
             'transaction_ref_no' => 'required|alpha_num',
             'amount_paid' => 'required|numeric',
             'image' => 'required|image|max:1200', 
             'sponsor_id' => 'required',
             'upline_id' => 'required',
             'package' => 'required',
             'position' => 'required',
             //'password' => 'required|confirmed|min:6',
                );
   $validator = Validator::make(Input::all(), $rules);
 
 if ( $validator->fails() ) 
        {
            return Redirect::back()->withInput()->withErrors( $validator );
        die();
        }

         $user = DB::table( 'members_info' )->where( 'member_id', $member_id )->first();
                if ( !$user ) {

                    ECHO 'NO USER';DIE();

                }
        //$title       = ucwords(Input::get('title'));
        $first_name       = ucwords(Input::get('first_name'));
        $last_name       = ucwords(Input::get('last_name'));
        $email       = Input::get('email');
        $invoice_no       = Input::get('invoice_no');
        $date_payment       = Input::get('date_payment');
        $bank       = Input::get('bank');
        $payment_method       = Input::get('payment_method');
        $transaction_ref_no       = Input::get('transaction_ref_no');
        $amount_paid       = Input::get('amount_paid');






 $sponsor_id = Request::input('sponsor_id');
                    $upline_id = Request::input('upline_id');
                    $position = Request::input('position');
                    $package = Request::input('package');
                    $password = Request::input('password');


                    if($package == 'regular')
                    {
                        $package_amount = "2000";
                    }elseif ($package == 'premium') {
                         $package_amount = "5000";
                    }elseif ($package == 'gold') {
                       $package_amount = "10000";
                    }else
                    {
                        echo 'error package amount';die();
                    }

                    /* for validation
                   $password  = md5( trim( Request::input( 'password' ) );
                    $valid_1 = DB::table( 'members_info' )->where( 'member_id', $member_id )->where( 'password', $password )->first();
                    if(!$valid_1)
                    {
                        echo 'error valid1';die();
                    }

                     $valid_2 = DB::table( 'members_info' )->where( 'active_buycode_id', $sponsor_id )->first();
                    if(!$valid_2)
                    {
                        echo 'error valid2';die();
                    }

                      $valid_3 = DB::table( 'members_info' )->where( 'active_buycode_id', $upline_id )->first();
                    if(!$valid_2)
                    {
                        echo 'error valid3';die();
                    }
                    for validation*/

                        /* insert to members downline*/
                    $data_downline = array(
                        "is_paid"=>0,//not yet paid dahil magreregister palang
                        "transaction_no"=>$transaction_ref_no,//not yet paid dahil magreregister palang
                        "member_id"=>$user->member_id,
                        "active_buycode_id"=>'',//wala pa dahil magreregister palang
                        "sponsor_buycode_id"=>$sponsor_id,
                        "upline_buycode_id"=>$upline_id,
                        "position"=>$position,
                        "package"=>$package,
                        "status"=>"0",
                        "created_at"=>date( 'Y-m-d H:i:s' )
                    );

                    $inserted_downline = DB::table( 'members_downline' )->insert( $data_downline );


                /*insert into member transaction*/
                    $data_downline = array(
                       // "bankdeposit_first_name"=>$first_name,//not yet paid dahil magreregister palang
                       // "bankdeposit_last_name"=>$last_name,//not yet paid dahil magreregister palang
                       // "bankdeposit_email"=>$email,
                        "bankdeposit_invoice_no"=>$invoice_no,//wala pa dahil magreregister palang
                        "bank"=>$bank,
                        "bankdeposit_transaction_ref_no"=>$transaction_ref_no,
                        "bankdeposit_date_payment"=>$date_payment,

                        "transaction_no"=>$transaction_ref_no,
                        "member_id"=> $user->member_id,
                        "buyDate"=>date( 'Y-m-d H:i:s' ),
                        "amount"=>$amount_paid,
                        "paymentStatus"=>"S",
                        "paymentDate"=>"",
                        "paymentGateway"=>$payment_method,
                        "ref_no"=>'0000',
                        "message"=>"Offline Payment Form Package Amount - ".$package_amount,
                    );
                    $inserted = DB::table( 'members_transaction' )->insert( $data_downline );

                    session()->put( 'success', [
                'message'=>'Please wait for the payment verification. Thank you. Will contact you via email and SMS by our administrator.'
                ] );

     return redirect('/firstdashboard');
    }


    private function generate_transaction_id( $email = "", $firstname = "", $fbid = "", $txnid = "" ) {
        if ( $email && $firstname && $fbid ) {
            return substr( sha1( $txnid.$email . $firstname . $fbid ), 0, 6 );
        } else {
            exit( "Error" );
        }
    }

    public function register() {
        $member_id = session( 'user' )['id'];

        $validator = $this->validator_payment( Request::all() );

        $message_fail = " fail ";
        $message_success = " success";
        if ( $validator->fails() ) {
            return Redirect::back()->withInput( Input::except( 'password' ) )->withErrors( $validator );
        }else {
            if ( $_POST ) {
            
                $last_item = DB::table( 'members_transaction' )->orderBy( 'id', 'desc' )->first();

                if ( $last_item ) {
                    $transaction_id = $last_item->id + 1;
                } else {
                    $transaction_id = 1;
                }
                $user = DB::table( 'members_info' )->where( 'member_id', $member_id )->first();
                if ( $user ) {

                    $sponsor_id = Request::input('sponsor_id');
                    $upline_id = Request::input('upline_id');
                    $position = Request::input('position');
                    $package = Request::input('package');
                    $password = Request::input('password');
                     $t_id = $this->generate_transaction_id( $user->email,  $user->first_name, $user->member_id.strtotime( date( 'Y-m-d' ) ), $transaction_id );


                    if($package == 'regular')
                    {
                        $package_amount = "2000";
                    }elseif ($package == 'premium') {
                         $package_amount = "5000";
                    }elseif ($package == 'gold') {
                       $package_amount = "10000";
                    }else
                    {
                        echo 'error package amount';die();
                    }

                     
                     $password  = md5( trim( $password));
                    $valid_1 = DB::table( 'members_info' )->where( 'member_id', $member_id )->where( 'password', $password )->first();
                    if(!$valid_1)
                    {
                         session()->put( 'error', [
                    'message'=>'Password and Logged in user not match'
                    ] );

                         return Redirect::back()->withInput( Input::except( 'password' ) );
                    }
                     if($sponsor_id != '0000000000')
                     {
                     $valid_2 = DB::table( 'members_info' )->where( 'active_buycode_id', $sponsor_id )->first();
                    if(!$valid_2)
                    {
                          session()->put( 'error', [
                    'message'=>'no sponsor_id in the database.'
                    ] );

                         return Redirect::back()->withInput( Input::except( 'password' ) );
                    }
                    }
                    if($upline_id != '0000000000')
                    {
                      $valid_3 = DB::table( 'members_info' )->where( 'active_buycode_id', $upline_id )->first();
                   
                    if(!$valid_2)
                    {
                        session()->put( 'error', [
                    'message'=>'no upline_id in the database.'
                    ] );

                         return Redirect::back()->withInput( Input::except( 'password' ) );
                    }
                    }
                    

                        /* insert to members downline*/
                    $data_downline = array(
                        "is_paid"=>0,//not yet paid dahil magreregister palang
                        "transaction_no"=>$t_id,//not yet paid dahil magreregister palang
                        "member_id"=>$user->member_id,
                        "active_buycode_id"=>'',//wala pa dahil magreregister palang
                        "sponsor_buycode_id"=>$sponsor_id,
                        "upline_buycode_id"=>$upline_id,
                        "position"=>$position,
                        "package"=>$package,
                        "status"=>"0",
                        "created_at"=>date( 'Y-m-d H:i:s' )
                    );

                    $inserted_downline = DB::table( 'members_downline' )->insert( $data_downline );

                   
                    // DRAGON PAY
                    $txnid = $t_id;
                    if ( in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1', '192.168.33.1' ) ) ) { //check if localhost or in staging / prod server
                        $url = DRAGONPAY_TEST_URL;
                        $merchant = DRAGONPAY_PRODUCTION_USER;
                        $passwd = DRAGONPAY_PRODUCTION_PASSWORD;
                    }else {
                        $url = DRAGONPAY_PRODUCTION_URL;
                        $merchant = DRAGONPAY_PRODUCTION_USER;
                        $passwd = DRAGONPAY_PRODUCTION_PASSWORD;
                    }
                    $amount = $package_amount.'.00';
                    $ccy = "PHP";
                    $description = $package." Package";
                    $email = $user->email;
                    $digest_str = "$merchant:$txnid:$amount:$ccy:$description:$email:$passwd";
                    $digest = sha1( $digest_str );
                    $params = "merchantid=" .   urlencode( $merchant ) .
                        "&txnid=" .         urlencode( $txnid ) .
                        "&amount=" .        urlencode( $amount ) .
                        "&ccy=" .           urlencode( $ccy ) .
                        "&description=" .       urlencode( $description ) .
                        "&email=" .         urlencode( $email ) .
                        "&digest=" .        urlencode( $digest );

                    $dragonpay_redirection = "$url?$params";
                    // DRAGON PAY

                    $data1 = array(
                        "transaction_no"=>$t_id,
                        "member_id"=>$user->member_id,
                        "buyDate"=>date( 'Y-m-d H:i:s' ),
                        "amount"=>$amount,
                        "paymentStatus"=>"",
                        "paymentDate"=>"",
                        "paymentGateway"=>"",
                        "ref_no"=>"",
                        "message"=>"Purchase only",
                        "digest"=>$digest
                    );

                    $inserted = DB::table( 'members_transaction' )->insert( $data1 );
                    //insert to API logs
                    // $string   = serialize( $data );
                    // $filename = "logs/logs.txt";
                    // $fp       = fopen( $filename, "a" );
                    // fwrite( $fp, date( "Y-m-d H:i:s" ) . "|| INSERT Registrants ".$t_id ."|| " . $string . "\r\n" );
                    // fclose( $fp );
                    
                    return redirect( $dragonpay_redirection );
                }else {
                    echo 'error';die();
                }

            }


        }
    }

    public function get_details_api( $refno ) {
        $requestParameters = array(
            'merchantId' => 'EPIC',
            'password' => 'V8rQE93w',
            'refNo' => $refno
        );

        $response = null;

        if ( in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1', '192.168.33.1' ) ) ) { //check if localhost or in staging / prod server
            $url = DRAGONPAY_TEST_API_URL;
        }else {
            $url = DRAGONPAY_PRODUCTION_API_URL;
        }


        // Create the SOAP request.
        $wsdl = new SoapClient( $url . '?WSDL',  array(
                'location' => $url,
                'trace' => 1,
            ) );

        //$response = $wsdl->GetMerchantTxns($requestParameters)->GetMerchantTxnsResult;
        $response = $wsdl->GetTxn( $requestParameters )->GetTxnResult;
        // Array inside "Transaction" key
        $payment_gateway = $response->procId;

        if ( $payment_gateway ) {
            return $response;
        }else {
            return FALSE;
        }


    }

    /**
     * Show the form for creating a new resource.
     * www.local.dev/laravel/public/returnurl?txnid=407b1f&refno=ATVB6573&status=P&message=%5b000%5d+Waiting+for+deposit+to+BogusBank+OTC+%23ATVB6573&digest=22cdaadc3d4b956685b97b76065e313b4290dbf6
     *
     * @return Response
     */
    public function returnurl() {

        if ( $_GET ) {

            $txnid = $_GET['txnid'];
            $refno = $_GET['refno'];
            $status = $_GET['status'];
            $message = $_GET['message'];

            //get api details.
            if ( extension_loaded( 'soap' ) ) {

                //$response = $this->get_details_api($refno);
                $response = false;
                if ( $response ) {
                    $param['paymentGateway'] = $response->procId;
                }else {
                    $param['paymentGateway'] = 'none';
                }

                //insert to API logs
                // $string   = serialize($response);
                // $filename = "logs/logs.txt";
                // $fp       = fopen($filename,"a");
                // fwrite($fp,date("Y-m-d H:i:s") . "|| GET DETAILS API ".$txnid ." || " . $string . "\r\n");
                // fclose($fp);


            }

            //get api details

            if ( isset( $txnid ) || isset( $refno ) ) {
                $transaction_details = DB::table( 'members_transaction' )->where( 'transaction_no', $txnid )->first();
                $get_registrant = DB::table( 'members_info' )->where( 'member_id', $transaction_details->member_id )->first();

                if ( $get_registrant ) {


                    $param['paymentStatus'] = $status;
                    $param['ref_no'] = $refno;
                    $param['paymentDate'] = date( 'Y-m-d H:i:s' );
                    $param['message'] = $message;

                    $boo =  DB::table( 'members_transaction' )->where( 'transaction_no', $txnid )->update( $param );
                    if ( $boo ) {
                        echo 'view success pending transaction.';
                    }else {
                        echo 'error';die();
                    }
                }
            }else {
                echo 'error';die();
            }

        }else {

        }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int     $id
     * @return Response
     */
    public function postback( ) {
        if ( $_POST ) { //not update this code the GET METHOD

        }else {
            if ( $_GET ) {
                echo 'GET MEthod';
                $txnid = $_GET['txnid'];
                $refno = $_GET['refno'];
                $status = $_GET['status'];
                $message = $_GET['message'];
                $param = '';
                if ( isset( $txnid ) ) {
                    $param['paymentStatus'] = $status;
                    $param['ref_no'] = $refno;
                    $param['paymentDate'] = date( 'Y-m-d H:i:s' );
                    $param['message'] = $message;

                    $getdetails = DB::table( 'members_transaction' )->where( 'transaction_no', $txnid )->first();

                    if ( $getdetails ) {
                        $boo =  DB::table( 'members_transaction' )->where( 'transaction_no', $txnid )->update( $param );
                        
                        if ( $boo ) {
                            if ( $status == "S" )//success payment
                                {
                    $param = array();
                    $param['is_paid'] = 1;
                    $param['updated_at'] = date( 'Y-m-d H:i:s' );
                  
 $boo =  DB::table( 'members_downline' )->where( 'transaction_no', $txnid )->update( $param );
        //sa finance approving page - add sa approved log - update sa member info ng active buycode
 // the message
$msg = "First line of text\nSecond line of text";

// use wordwrap() if lines are longer than 70 characters
$msg = wordwrap($msg,70);

// send email
mail("jerome.jose@nuworks.ph","My subject",$msg);

 // $content = "HI HI HI";
 // $email_array = array();
 //    $email1 =  Mail::send('emails.contactUs', ['name'=>'User','body_message'=>trim($content)] , function($message)
 //                                         Use ($email_array)
 //                                    {
 //                                        $message->subject('subject');

 //                                        $message->from('jerome.jose@nuworks.ph');

 //                                        $message->to('jerome.jose@nuworks.ph');//for prod
 //                                       // $message->to('jerome.jose@nuworks.ph');
 //                                    });

                                echo 'email user \n alert admin to approved the buycode';

                            }else {

                            }
                        }else {
                            echo 'error updated already';die();
                        }
                    }

                }else {
                    echo 'error';die();
                }


            }
            echo 'error';die();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int     $id
     * @return Response
     */
    public function edit( $id ) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     * @return Response
     */
    public function update( Request $request, $id ) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int     $id
     * @return Response
     */
    public function destroy( $id ) {
        //
    }

     public function bank_deposit() {
        
    }

}
